#####
## Base image for Soter ASGI apps
#####

FROM python:3.7-alpine

# Create a user to use to run the ASGI server
ENV ASGI_UID 1000
ENV ASGI_GID 1000
ENV ASGI_USER asgi
ENV ASGI_GROUP asgi
RUN addgroup --gid $ASGI_GID $ASGI_GROUP && \
    adduser \
      --no-create-home \
      --shell /sbin/nologin \
      --disabled-password \
      --uid $ASGI_UID \
      --ingroup $ASGI_GROUP \
      $ASGI_USER

# Install Tini
RUN apk add --no-cache tini=0.19.0-r0

# Use hypercorn as our ASGI server
ARG HYPERCORN_VERSION=0.11.1
RUN pip install "hypercorn==${HYPERCORN_VERSION}"

# Install configuration and scripts
COPY hypercorn.toml /etc/hypercorn/
COPY asgi-run.sh /

# Use Tini as the entrypoint
ENTRYPOINT ["/sbin/tini", "-g", "--"]
# By default, run the ASGI app
CMD ["/asgi-run.sh"]
