#!/bin/sh

#####
## Entrypoint that just starts hypercorn
#####

set -e

exec hypercorn --config /etc/hypercorn/hypercorn.toml "$@"
